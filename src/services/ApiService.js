import axios from 'axios';
import Story from '../entities/Story';
import Comment from '../entities/Comment'


const TOP_STORIES_ENDPOINT = `https://hacker-news.firebaseio.com/v0/topstories.json`;
const SINGLE_STORY_ENDPOINT = `https://hacker-news.firebaseio.com/v0/item/`;
const COMMENT_ENDPOINT = `https://hacker-news.firebaseio.com/v0/item/`;
class ApiService {

    static fetchTopStories = () => {
      return  axios.get(TOP_STORIES_ENDPOINT)
        .then(response => response.data)
    }

    static fetchStories = (id) => {
        const requests = id.map(id => axios.get(SINGLE_STORY_ENDPOINT + `${id}.json`));
        return Promise.all(requests)
        .then(responses =>  responses.map(response =>  new Story(response.data)))
    }

    static fetchSingleStory = (id) => {
        return axios.get(SINGLE_STORY_ENDPOINT + `${id}.json`)
        .then(response => new Story(response.data))
    }

    static fetchComments = (id) => {
        const requests = id.map((id) => {
            return axios.get(COMMENT_ENDPOINT + `${id}.json`)
         });
         return Promise.all(requests)
         .then(responses =>  responses.map(response => new Comment(response.data || {})))
    }
}

    export default ApiService;
