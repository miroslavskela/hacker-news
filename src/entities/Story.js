class Story {
  constructor(story) {
    this.by = story.by;
    this.descedants = story.descedants;
    this.id = story.id;
    this.kids = story.kids || [];
    this.score = story.score;
    this.text = story.text;
    this.time = story.time;
    this.title = story.title;
    this.type = story.type;
    this.url = story.url;
  }
}

export default Story;
