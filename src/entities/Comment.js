class Comment {
  constructor(comment) {
    this.by = comment.by;
    this.id = comment.id;
    this.kids = comment.kids || [];
    this.parent = comment.parent;
    this.text = comment.text || "Comment is deleted";
    this.time = comment.time;
    this.type = comment.type;
  }
  getTime(){
    const time = new Date(this.time)
    const day = time.getDate();
    const month = time.getMonth() + 1;
    const year = time.getFullYear();
      
    return `${month}.${day}.${year}`;
  }
}

export default Comment;
