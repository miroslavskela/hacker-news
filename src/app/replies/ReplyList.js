import React, { Fragment } from "react";
import ReplyItem from "./ReplyItem";
import './Replies.css';

const ReplyList = ({ data }) => {
  return (
    <Fragment>
      <div className="reply-container">
        {data.map((reply, index) => <ReplyItem data={reply} index={index} key={index} />)}
      </div>
    </Fragment>
  );
};

export default ReplyList;
