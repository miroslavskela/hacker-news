import React, {Component, Fragment} from 'react';
import CommentList from './CommentList';
import ApiService from '../../services/ApiService';
import Loader from '../loader/Loader'
import {Link} from 'react-router-dom'

class MainCommentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      singleStory: {},
      comments: [],
      error: "",
      loader: true,
      start:0,
      stop:10,
      more:true,
    };
    
  }
  
  fetchComments = () => {
    const {comments, start,stop} = this.state;
    const ids = JSON.parse(localStorage.getItem("ids"));
    const slicedIds = ids.slice(start,stop)
    ApiService.fetchComments(slicedIds)
    .then(loadedComments => {
      this.setState({comments: [...comments, ...loadedComments],loader: false,});
      this.checkRepliesState(ids, this.state.comments)
    })
    .catch(error => {
      this.setState({ error: error, loader:false });
    });
  };
  
  componentDidMount() {
    ApiService.fetchSingleStory(this.props.match.params.id).then(singleStory =>
      this.setState({ singleStory:singleStory }, this.fetchComments)
    );
  }
  loadMore = () => {
    this.setState(
      prevState => ({
        start: prevState.start + 10,
        stop: prevState.stop + 10,
      }),this.fetchComments 
    );
  };

  checkRepliesState = (ids, comments) => {
    if(comments.length === ids.length){
      this.setState({more:false})
    }
  }
  
  
  render() {
   this.ids = JSON.parse(localStorage.getItem('ids'))
   if (this.state.comments.length === 0) {
    return <Loader />;
  }
    return (
      <Fragment>
        <Link to="/"><span className="home-page">Home page</span></Link>
       <CommentList data={this.state.comments} data1={this.state.singleStory}/>
        {this.state.more?<span className="center-align load-more" onClick={this.loadMore}>More</span>:null}
        {this.state.error?<h2 className="center-align">{this.state.error.message}</h2>:null}
      </Fragment>
    );
  }
}

export default MainCommentPage