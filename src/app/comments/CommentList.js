import React, { Fragment } from "react";
import CommentItem from "./CommentItem";
import './Comment.css';

const CommentList = ({ data, data1 }) => {
  return (
    <Fragment>
      <div className="row">
      <div className="container comment-container">
      <p className="story-title-comments"><a href={data1.url} target=":_blank">{data1.title} <span className="story-url">({data1.url})</span></a> </p>
        {data.map((comment, index) =>  <CommentItem data={comment} index={index} key={index} />)}
      </div>
      </div>
    </Fragment>
  );
};

export default CommentList;
