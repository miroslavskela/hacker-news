import React, { Component, Fragment } from "react";
import ReplyList from "../replies/ReplyList";
import ApiService from '../../services/ApiService';
import TimeAgo from 'react-timeago'
import './Comment.css';

class CommentItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      replies: [],
      repPage: false,
      error: ""
    };
  }

  fetchReplies = () => {
     const {kids} = this.props.data;
    const {replies} = this.state
     if (replies.length !== 0) {
      this.setState({ repPage: true });
      return;
    }
    if(kids.length!==0){
    ApiService.fetchComments(kids)
    .then((loadedReplies) => {
        this.setState({replies:loadedReplies, repPage:true})
    })
        .catch((error) => {
          this.setState({ error: error });
        })
      }
    }
   
  closeReplies = () => {
    this.setState({ repPage: false });
  };

  render() {
    return (
      <Fragment>
        <div className="comment-div">
          <span className="title">{this.props.index+1}. {this.props.data.by} <span className="timeago"><TimeAgo date={this.props.data.getTime()}/></span> </span>
          <p className="comment-text">{this.props.data.text}</p>
          <a className="waves-effect waves-light btn-small" onClick={this.fetchReplies}> 
        Replies: {this.props.data.kids.length}
        </a>
          {this.state.repPage?<a className="waves-effect waves-light btn-small btn1" onClick={this.closeReplies}> Close Replies</a>:null}
          {this.state.repPage?<ReplyList data={this.state.replies} />:null}
        </div>
        {this.state.error?<h2 className="center-align">{this.state.error}</h2>:null}
      </Fragment>
    );
  }
}



export default CommentItem;
