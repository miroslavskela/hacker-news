import React, { Component } from "react";
import './Search.css'

class Search extends Component {
  constructor(props) {
    super();
  }
  state = {
    value: ""
  };
  handleChange = event => {
    const inputValue = event.target.value;
    this.props.onSearchValueChange(inputValue);
    this.setState({ value: inputValue });
  };
  render = () => {
    return (
      <div className="topnav">
        <input value={this.state.value} onChange={this.handleChange} id="search-input" type="text" placeholder="Search.."/>
      </div>
    );
  };
}
export default Search;
