import React from "react";

const Header = () => {
  return (
    <nav>
      <div className="nav-wrapper">
        <a href="" className="brand-logo center">
          Hacker News
        </a>
      </div>
    </nav>
  );
};

export default Header;
