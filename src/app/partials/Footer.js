import React from "react";

const Footer = () => {
  return (
    <footer className="page-footer">
      <div className="footer-copyright">
        <div className="container center-align">© Miroslav Skeledzija</div>
      </div>
    </footer>
  );
};

export default Footer;
