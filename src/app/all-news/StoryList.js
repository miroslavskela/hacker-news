import React from "react";
import StoryItem from "./StoryItem";
import NoStory from "./NoStory";
import './Story.css'

const StoryList = ({ data, getComments, comPage, comments, i }) => {
  const getStoryList = () => {
    if (data.length !== 0) {
      return data.map((story, index) => <StoryItem data={story} index={index} key={index} />)
    } else {
      return <NoStory />;
    }
  };
  return (
    <div className="row">
      <div className="container" id="main-container">
        <h3 className="center-align"> TOP STORIES </h3>
        {getStoryList()}
      </div>
    </div>
  );
};

export default StoryList;
