import React, { Component, Fragment } from "react";
import ApiService from '../../services/ApiService'
import StoryList from "./StoryList";
import Search from "../partials/Search";
import Loader from "../loader/Loader";
import './Story.css'

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topStories:[],
      stories: [],
      error: "",
      searchValue: "",
      start: 0,
      stop: 10,
      more:true,
    };
  }

  storyRequest = () => {
    const { stories, topStories, start, stop } = this.state;
    const iDs = topStories.slice(start,stop);

    ApiService.fetchStories(iDs)
    .then((loadedStories) => {
        this.setState({ stories: [...stories, ...loadedStories], scrolling: false})
        this.checkStoriesState(topStories, this.state.stories)
    })
      .catch(error => {
        this.setState({ error: error });
      });
  };


  loadMore = () => {
    if(this.state.start >= this.state.topStories){
      return
    }
    this.setState(
      prevState => ({
        start: prevState.start + 10,
        stop: prevState.stop + 10,
      }),this.storyRequest 
    );
  };

  fetchTopStories = () => {
    ApiService.fetchTopStories()
    .then((topStories) => {
        this.setState({topStories:topStories}, this.storyRequest)
    });
  }
  checkStoriesState = (topStories, stories) => {
    if(topStories.length === stories.length){
      this.setState({more:false})
    }
  }

  componentDidMount() {
      this.fetchTopStories() 
  }

  searchValueChange = value => {
    this.setState({ searchValue: value });
  };

  getStories = () => {
    const filterStories = this.state.stories.filter(story => {
      return story.title.toLowerCase().includes(this.state.searchValue.toLowerCase());
    });
    return filterStories;
  };

  render() {
    if (this.state.stories.length === 0) {
      return <Loader />;
    }
    return (
      <Fragment>
        <Search onSearchValueChange={this.searchValueChange} />
        <StoryList data={this.getStories()} />
        {this.state.more?<span className="center-align load-more" onClick={this.loadMore}>More</span>:null}
        {this.state.error?<h2 className="center-align">{this.state.error}</h2>:null}
      </Fragment>
    );
  }
}

export default MainPage;
