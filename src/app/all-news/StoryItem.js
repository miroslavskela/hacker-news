import React, { Fragment } from "react";
import {Link} from 'react-router-dom'
import './Story.css'

const StoryItem  = ({data}) =>  {
 
  const saveIds = () => {
    localStorage.setItem('ids',JSON.stringify(data.kids))
  }

    return (
      <Fragment>
        <div className="story-card">
          <div className="card">
            <div className="card-content white-text">
              <span className="card-title">
                <a className="story-title" href={data.url} target="_blank">
                  {data.title}
                </a>
              </span>
              <p className="author">Author: {data.by}</p>
            </div>
            <div className="card-action">
              <p className="score">Score: {data.score}</p>
              {data.kids.length !== 0?<Link to={`/comments/id:${data.id}`}><span onClick={saveIds}className="comments">
                Comments: {data.kids.length}
              </span></Link>:<span className="comments">
                Comments: 0
              </span>}
              
            </div>
          </div>
        </div>
      </Fragment>
    );
  }


export default StoryItem;
