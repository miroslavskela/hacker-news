import React, { Fragment } from "react";

const NoStory = props => {
  return (
    <Fragment>
      <div className="container">
        <div className="row">
          <h1 className="center-align">  
            Sorry, we couldn't find any story matching your search
          </h1>
        </div>
      </div>
    </Fragment>
  );
};
export default NoStory;
