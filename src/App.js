import React, {Fragment, Component } from 'react';
import './css/App.css';
import Header from './app/partials/Header'
import Footer from './app/partials/Footer'
import MainPage from './app/all-news/MainPage'
import {Switch, Route} from 'react-router-dom'
import MainCommentPage from './app/comments/MainCommentPage'

class App extends Component {
  render() {
    return (
     <Fragment>
       <Header/>
       <Switch>
       <Route exact path="/" component={MainPage}/>
       <Route exact path={'/comments/id::id'} component={MainCommentPage}/>
       </Switch>
       <Footer/>
     </Fragment>
    );
  }
}

export default App;
